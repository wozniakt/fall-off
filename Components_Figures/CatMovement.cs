﻿using UnityEngine;
using System.Collections;

public class CatMovement : MonoBehaviour
{
	public Vector4 moveVariables;
	// Use this for initialization
	Rigidbody2D catRigidbody2d;
	void Start ()
	{
		catRigidbody2d = GetComponent<Rigidbody2D> ();
		StartCoroutine (move ());

	}
	
	// Update is called once per frame
	IEnumerator move ()
	{	

		yield return new WaitForSeconds (1);
		catRigidbody2d.velocity = new Vector2 (moveVariables.x, moveVariables.y);

		yield return new WaitForSeconds (Random.Range(1,15));
		catRigidbody2d.velocity = new Vector2 (-moveVariables.x, moveVariables.y);

		yield return new WaitForSeconds (Random.Range(1,3));

		catRigidbody2d.velocity = new Vector2 (Random.Range(-moveVariables.x,moveVariables.x), moveVariables.y);
		catRigidbody2d.AddForce ( new Vector2 (0, moveVariables.w));
		Debug.Log("CAT ADD FORce "+catRigidbody2d.velocity.x);
		SoundsManager.Instance.PlaySound (SoundType.Meow);
		yield return new WaitForSeconds (moveVariables.z);
		StartCoroutine (move());

	}
}

