﻿using UnityEngine;
using System.Collections;

public class UISpecialFigure : MonoBehaviour
{
	Vector2 startPos;
	// Use this for initialization
	void OnEnable ()
	{
		startPos = transform.localPosition;
		//transform.localPosition = startPos;
		GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
	
	}
	void OnDisable ()
	{
		transform.localPosition = startPos;
		GetComponent<Rigidbody2D> ().velocity = Vector2.zero;

	}
	
	// Update is called once per frame
	void Start ()
	{
		startPos = transform.localPosition;
	}
}

