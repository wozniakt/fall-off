﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ImageFiller : MonoBehaviour
{
	Image img;
	public float timeToFill;
	// Use this for initialization
	void OnEnable ()
	{

		img = GetComponent<Image> ();
		StartCoroutine (fillOut (timeToFill));
	}
	
	// Update is called once per frame
	IEnumerator fillOut (float timeToFillOut)
	{

		for (float i = 1; i >-1 ; i=i-(0.015f/timeToFillOut)) {
			//Debug.Log (i);
			img.fillAmount = i;
			yield return new WaitForSeconds (0.0015f/timeToFillOut);
		}
	}
	void OnDisable(){

		StopAllCoroutines ();
	}

}

