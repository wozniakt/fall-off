﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PoolManager : MonoBehaviour
{

	public static string POOLED_PREFAB_PATH = "Prefabs/";
	public static PoolManager instance;
	public GameObject FiguresPool, EffectsPool;
	public bool WillGrow = true;


	List<GameObject> pooledFigures;
	List<GameObject> pooledEffects;

	void Awake()
	{  
		instance = this;
		pooledFigures = new List<GameObject>();
		pooledEffects = new List<GameObject>();

		createFigures(2,0);
		createFigures(2,1);
		createFigures (2, 2);
		createFigures(2,3);
		createFigures(2,4);
		createFigures(2,5);
		createFigures(2,6);
//		createFigures(2,7);
//		createFigures(2,8);
		createFigures(4,100);

		createEffects (3, EffectType.RedSplash);
		createEffects (3, EffectType.PinkPigExplosion);
		createEffects (3, EffectType.Coins);
		// tu mzoesz zrobic dynamciznie na podstawie slected character bullets
	}

	void createFigures(int count, int randomFigureNumber)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Figures_Prefabs/Figure"+randomFigureNumber.ToString()));

			obj.SetActive(false);
			obj.name = "Figure"+randomFigureNumber.ToString();
			pooledFigures.Add(obj);
			obj.transform.SetParent(FiguresPool.transform);
		}

	}

	//
	public GameObject GetPooledObject_Figure(int randomFigureNumber)
	{
		foreach (GameObject item in pooledFigures)
		{
			if (item.name == "Figure"+randomFigureNumber.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} 
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Figures_Prefabs/Figure"+ randomFigureNumber));
			obj.name="Figure"+randomFigureNumber.ToString();
			pooledFigures.Add(obj);
			obj.transform.SetParent(FiguresPool.transform);
			return obj;
		}
		return null;
	}

	void createEffects(int count, EffectType effectType)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Effects_Prefabs/"+effectType.ToString()));
			obj.SetActive(false);
			obj.name = effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
		}

	}

	//
	public GameObject GetPooledObject_Effect(EffectType effectType)
	{
		foreach (GameObject item in pooledEffects)
		{
			if (item.name == effectType.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Effects_prefabs/"+ effectType.ToString()));
			obj.name=effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
			return obj;
		}
		return null;
	}



}




