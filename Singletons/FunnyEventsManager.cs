﻿using UnityEngine;
using System.Collections;

public class FunnyEventsManager : MonoBehaviour
{
	public static string PREFAB_PATH = "Prefabs/";
	public static FunnyEventsManager instance;
	FunnyEvent randomFunnyEvent;
	DataManager dataManager;
	PlayerData oPlayerData;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}
	void Start(){
		dataManager = DataManager.instance;
		oPlayerData = DataManager.instance.oPlayerData;
		ManagerOfGlobalEvents.instance.eAchievementUnlock += this.unlockAchievement;
		ManagerOfGlobalEvents.instance.eDataChange += this.UpdateFunnyEvents;
		UpdateFunnyEvents ();
	}

	void UpdateFunnyEvents(){
		foreach (FunnyEvent oFunnyEvent in oPlayerData.FunnyEventsList) {
			if (oFunnyEvent.Number<=Mathf.Max((oPlayerData.HighscorePointsCount / 10),2)) {
				oFunnyEvent.IsUnlocked = true;
			}
		}
	}

	public void RandomizeEventNumber (){

		randomFunnyEvent = oPlayerData.CurrentFunnyEvent;
	//	randomFunnyEvent =oPlayerData.FunnyEventsList[ Random.Range (1, oPlayerData.FunnyEventsList.Count)];


		if (oPlayerData.CoinsCount>dataManager.eventsCost) {

		
			randomFunnyEvent=oPlayerData.FunnyEventsList[Random.Range(1,oPlayerData.FunnyEventsList.Count)];

			if (randomFunnyEvent.Number!=oPlayerData.CurrentFunnyEvent.Number && randomFunnyEvent.IsUnlocked) {
					oPlayerData.CurrentFunnyEvent = randomFunnyEvent;

			} else {
						//oPlayerData.CurrentFunnyEvent = randomFunnyEvent;
				for (int i = oPlayerData.FunnyEventsList.Count-1; i >0; i--) {
						if (oPlayerData.FunnyEventsList[i].IsUnlocked && oPlayerData.FunnyEventsList[i].Number!=oPlayerData.CurrentFunnyEvent.Number) {
						randomFunnyEvent=oPlayerData.FunnyEventsList[i];
						i = 0;
					}

				}

		}
			SoundsManager.Instance.PlaySound (SoundType.Button);
			oPlayerData.CurrentFunnyEvent = randomFunnyEvent;
			oPlayerData.CoinsCount = oPlayerData.CoinsCount - dataManager.eventsCost;
			ManagerOfGlobalEvents.instance.TriggerDataChange();
			ManagerOfGlobalEvents.instance.TriggerChangeEventImage (oPlayerData.CurrentFunnyEvent.Number.ToString ());
		} else {
			SoundsManager.Instance.PlaySound (SoundType.NotEnoughMoney);
			ManagerOfGlobalEvents.instance.TriggerNotEnoughMoney("You have not enough money!");
		}

	}


	void unlockAchievement(FunnyEvent funnyEvent){
		if (funnyEvent.IsUnlocked==false) {
			funnyEvent.IsUnlocked = true;
			UImanager.instance.ShowUnlockedAchievement (funnyEvent);
		}
	
	}
	// Update is called once per frame
	public void doEvent (int eventNumber)
	{
		dataManager.coinMultipler = 1;


		switch (eventNumber) {
		case 0://standard mode
			initialGameData (8,"StandardBG",1,3);
			break;
		case 1://blades mode
			initialGameData (7,"StandardBG",2,3);
			GameObject blades = (GameObject)Instantiate (Resources.Load ("Prefabs/Events_Prefabs/Event_" + "Blades"));
			blades.transform.SetParent (GameObject.Find ("PlaceHolder_Blades").transform);
			blades.transform.position = GameObject.Find ("PlaceHolder_Blades").transform.position;
			break;
		case 2://additional building
			initialGameData (6,"StandardBG",1,3);
			GameObject building3 = (GameObject)Instantiate (Resources.Load (PREFAB_PATH + "Buildings_Prefabs/" + "Building3"));
			building3.transform.SetParent (GameObject.Find ("PlaceHolder_Building3").transform);
			building3.transform.position = GameObject.Find ("PlaceHolder_Building3").transform.position;
			break;
		case 3://four lives
			initialGameData (7, "StandardBG", 1, 4);
			break;
		case 4://cloudscloudscloudscloudscloudsclouds
			initialGameData (7,"StandardBG",2,3);
			GameObject clouds = (GameObject)Instantiate (Resources.Load ("Prefabs/Events_Prefabs/Event_" + "Clouds"));
			clouds.transform.SetParent (GameObject.Find ("PlaceHolder_Clouds").transform);
			clouds.transform.position = GameObject.Find ("PlaceHolder_Clouds").transform.position;

			break;

		case 5://red sky
			initialGameData (7,"RedSky",1,3);
			break;

		case 6://faster more money
			initialGameData (5,"StandardBG",3,3);
			break;

		case 7://yolo 
			initialGameData (7,"StandardBG",3,1);
			break;

		case 8://9 lives cats
			initialGameData (7,"StandardBG",3,9);
			GameObject nineLives = (GameObject)Instantiate (Resources.Load ("Prefabs/Events_Prefabs/Event_" + "NineLives"));
			nineLives.transform.SetParent (GameObject.Find ("PlaceHolder_NineLives").transform);
			nineLives.transform.position = GameObject.Find ("PlaceHolder_NineLives").transform.position;
			break;
	}
}

	void initialGameData(int interval, string bgName, int coinMultipler,int playerHp){
		DataManager.instance.intervalFiguresSpawner = interval;
		UImanager.instance.ChangeBackground(bgName);
		dataManager.coinMultipler = coinMultipler;
		oPlayerData.HpCount = playerHp;
	}

}