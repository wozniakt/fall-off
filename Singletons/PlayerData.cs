﻿using System;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerData 
{
	int maxFunnyEventToUnlock,newFunnyEventToUnlockNo;
	FunnyEvent newFunnyEventToUnlock;
	[SerializeField]
	float comboCount;
	public float ComboCount
	{
		get
		{return comboCount;}
		set{

			comboCount= value;

		}
	}

	[SerializeField]
	float comboCount2;
	public float ComboCount2
	{
		get
		{return comboCount2;}
		set{

			comboCount2= value;

		}
	}
		

	[SerializeField]
	int pointsCount;
	public int PointsCount
	{
		get
		{return pointsCount;}
		set{

			pointsCount= value;

		}
	}

	[SerializeField]
	List<FunnyEvent> funnyEventsList;
	public List<FunnyEvent> FunnyEventsList{
		get
		{return funnyEventsList;}
		set{

			funnyEventsList = value;  //new List<Creature> (DataManager.instance.heroesList);
			//		coinsCount = DataManager.instance.coins;
		}
	}


	[SerializeField]
	FunnyEvent currentFunnyEvent;
	public FunnyEvent CurrentFunnyEvent
	{
		get
		{return currentFunnyEvent;}
		set{
			currentFunnyEvent= value;
		}
	}
	[SerializeField]
	int maxUnlockedFunnyEventNo;
	public int MaxUnlockedFunnyEventNo
	{
		get
		{return maxUnlockedFunnyEventNo;}
		set{
			if (value==0) {
				value = 1;
			}
			maxUnlockedFunnyEventNo= value;

		}
	}
	[SerializeField]
	int highscorePointsCount;
	public int HighscorePointsCount
	{
		get
		{return highscorePointsCount;}
		set{
			if (highscorePointsCount<value) {
			highscorePointsCount= value;
				newFunnyEventToUnlockNo=
					Mathf.Min(( Mathf.RoundToInt(HighscorePointsCount / 10)+2),
						DataManager.instance.oPlayerData.funnyEventsList.Count-1);
				maxFunnyEventToUnlock = DataManager.instance.oPlayerData.funnyEventsList.Count;
				if (newFunnyEventToUnlockNo<maxFunnyEventToUnlock) {
					ManagerOfGlobalEvents.instance.TriggerAchievementUnlock	(DataManager.instance.oPlayerData.funnyEventsList[newFunnyEventToUnlockNo]);
				DataManager.instance.oPlayerData.MaxUnlockedFunnyEventNo = newFunnyEventToUnlockNo;
				}
			
			}

		}
	}


	[SerializeField]
	int coinsCount;
	public int CoinsCount
	{
		get
		{return coinsCount;}
		set{
			if (value>=0) {
				coinsCount= value;
			} else {
				coinsCount = 0;		
			}	

			PlayerPrefs.SetInt ("CoinsCount", CoinsCount);
		}
	}

	[SerializeField]
	int hpCount;
	public int HpCount
	{
		get
		{return hpCount;}
		set{
			hpCount= value;

		}
	}

}