﻿using UnityEngine;
using System.Collections;

public enum GameState {Menu, GameOn, GamePause, GameLost}

public enum FigureType {Human, Human1,Human2, Human3}

public enum EffectType {RedSplash, CanvasCloud, HappyJump, PinkPigExplosion, Coins}

public enum SoundType
{
	Splash, Coins, Button, Reflect, Jump, Pig, Rescued, PigBroken,Applause, Meow,NotEnoughMoney
}


