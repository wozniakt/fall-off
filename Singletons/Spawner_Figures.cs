﻿	using UnityEngine;
using System.Collections;

public class Spawner_Figures : MonoBehaviour
{
	public float intervalSpawn;
	Vector2 placeHolder_Figures;
	int DownBound, UpperBound;
	int randomFigureNumber;
	public int maxFigureNumber;
	public string placeHolderName;
	public int newFigureTypeBound;
	// Use this for initialization
	void Start ()
	{
		
		intervalSpawn = DataManager.instance.intervalFiguresSpawner;
		placeHolder_Figures= GameObject.Find (placeHolderName).transform.position;
		StartCoroutine (spawnFigure (intervalSpawn, DownBound,  DownBound+2));
	}
	

	IEnumerator spawnFigure(float interval, int DownBound, int UpperBound){
		yield return new WaitForSeconds (interval/2);
		randomFigureNumber = (int)(Random.Range (DownBound, DownBound+2));

		GameObject figure=PoolManager.instance.GetPooledObject_Figure (randomFigureNumber);
		figure.transform.position=placeHolder_Figures;
		figure.SetActive (true);
	//	Debug.Log ("Figure: " + randomFigureNumber);
		SoundsManager.Instance.PlaySound (SoundType.Jump);
		yield return new WaitForSeconds (interval/2);
		DownBound=(Mathf.RoundToInt(DataManager.instance.oPlayerData.PointsCount/newFigureTypeBound));
		DownBound = Mathf.Min (DownBound, maxFigureNumber - 1);
		//Debug.Log ("DownBound: " + DownBound+"DataManager.instance.oPlayerData.PointsCount/100: "+DataManager.instance.oPlayerData.PointsCount/100);
		if (DataManager.instance.oGameState==GameState.GameOn) {
			//intervalSpawn = DataManager.instance.intervalFiguresSpawner;
				StartCoroutine (spawnFigure (intervalSpawn,DownBound,DownBound+2));
		}

	}
}

