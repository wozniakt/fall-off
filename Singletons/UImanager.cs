﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class UImanager : MonoBehaviour
{
	ManagerOfGlobalEvents managerOfGlobEvents;

	public Text ComicClooudInMenuTxt;
	public static string PREFAB_PATH = "Prefabs/";
	public Text coinsText, pointsText,highscoreText,FunnyEventDescriptionText;
	public Text comboText,achievementText;
	public GameObject GameBackground, GameOverTextAsImage;
	public  Image imgHp, imgAchievement,img_RandomEvent, ImageEvent_Visualization;
	public GameObject joystick;
	public GameObject PanelPause, PanelMenu, GameOver_prefab, PanelBackground;
	public Button ButtonMenu_SoundOff,ButtonMenu_MusicOff;
	public Button ButtonMenu_ReStart,  ButtonPause_Resume,ButtonPause_Pause, ButtonPause_RestartGame, ButtonPause_BackToMenuAfterLost,ButtonPause_BackToMenu;
	public Button ButtonMenu_RandomEvent, ButtonQuick_Restart;
	public Button ButtonMenu_WatchAdd, ButtonMenu_Exit;
	public GameObject Menu_Title;
	public GameObject PanelQuick_Restart;
	public static UImanager instance;
	Animator Img_RandomEvent_Animator,PanelBackground_Animator;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}	
		DontDestroyOnLoad (instance.gameObject);
	}
		
	void Start ()
	{	managerOfGlobEvents = ManagerOfGlobalEvents.instance;
		
		managerOfGlobEvents.eChangeEventImage += this.changeEventImage;
		managerOfGlobEvents.eNotEnoughMoney += this.ShowTextInComicCloud;
		managerOfGlobEvents.eDataChange += this.UpdateEventDescript;
		managerOfGlobEvents.eDataChange += this.UpdateHudTexts;
		managerOfGlobEvents.OnChangeGameState += this.UIChangeGameState;
		managerOfGlobEvents.eDataChange += this.UpdateComboText;
		managerOfGlobEvents.eDataChange += this.UpdateHpText;
		managerOfGlobEvents.eDataChange += this.UpdateComboText;
		//managerOfGlobEvents.eAchievementUnlock += this.ShowUnlockedAchievement;
		createScene ();

		AddListenersToButtons ();
		Img_RandomEvent_Animator = img_RandomEvent.GetComponent<Animator> ();
		PanelBackground_Animator = PanelBackground.GetComponent<Animator> ();
		managerOfGlobEvents.TriggerOnChangeGameState (GameState.Menu);
		managerOfGlobEvents.TriggerDataChange ();
	}

	public void createScene(){
		// aaahhh co sie tu dzieje xD?
		// scaszuj se te placeholdery
		GameObject ground = (GameObject)Instantiate(Resources.Load(PREFAB_PATH +"Other_Prefabs/"+"Ground"));
		ground.transform.SetParent ( GameObject.Find ("PlaceHolder_Ground").transform);
		ground.transform.position= GameObject.Find ("PlaceHolder_Ground").transform.position;

		GameObject trampoline = (GameObject)Instantiate(Resources.Load(PREFAB_PATH +"Figures_Prefabs/"+"Trampoline"));
		trampoline.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
		trampoline.transform.SetParent ( GameObject.Find ("PlaceHolder_Trampoline").transform);
		trampoline.transform.position= GameObject.Find ("PlaceHolder_Trampoline").transform.position;

		GameObject building = (GameObject)Instantiate(Resources.Load(PREFAB_PATH +"Buildings_Prefabs/"+"Building1"));
		building.transform.SetParent( GameObject.Find ("PlaceHolder_Building").transform);
		building.transform.position= GameObject.Find ("PlaceHolder_Building").transform.position;

		GameObject building2 = (GameObject)Instantiate(Resources.Load(PREFAB_PATH +"Buildings_Prefabs/"+"Building2"));
		building2.transform.SetParent( GameObject.Find ("PlaceHolder_Building2").transform);
		building2.transform.position= GameObject.Find ("PlaceHolder_Building2").transform.position;


	}

	public void ChangeBackground(string spriteName){

		Sprite newSprite =(Resources.Load<Sprite> ("Prefabs/Events_Prefabs/Event_"  + spriteName));
		GameBackground.GetComponent<SpriteRenderer> ().sprite = newSprite;
	}

	public void  UpdateHudTexts(){
		// po co ten parametr skoro go nie wykorzstujesz??
		// powinienes wlasnie update zrobic na podstawie parametru.
		// Osobne : UpdateCoinsText i UpdatePointsText, chyba ze wiesz, a to jest "póki co"
			coinsText.text = "COINS: "+DataManager.instance.oPlayerData.CoinsCount;
			pointsText.text = "POINTS: " + DataManager.instance.oPlayerData.PointsCount;
			highscoreText.text="HIGHSCORE: "+DataManager.instance.oPlayerData.HighscorePointsCount;
	}

	public void  ShowUnlockedAchievement(FunnyEvent funnyEvent){
		imgAchievement.gameObject.SetActive(true);
		achievementText.text = "EVENT UNLOCKED \n"  + funnyEvent.Description;
	}

	public void  UpdateHpText(){
		imgHp.fillAmount = (DataManager.instance.oPlayerData.HpCount/10f);
	}



	public void  UpdateComboText(){
		if (DataManager.instance.oPlayerData.ComboCount > 1) {
			comboText.text = "COMBO x " + DataManager.instance.oPlayerData.ComboCount;
		} else {
			comboText.text = "";
		}
	}

	void AddListenersToButtons(){
		// spoko, ale mozna tez na scenie, ale tak chyba nawet lepiej,
		//bo wiesz gdzie masz te przypisania a na scenie czasami zniknie referncja
		ButtonMenu_ReStart.onClick.RemoveAllListeners ();
		ButtonMenu_ReStart.onClick.AddListener (() => DataManager.instance.ReStartGame ());

		ButtonQuick_Restart.onClick.RemoveAllListeners ();
		ButtonQuick_Restart.onClick.AddListener (() => DataManager.instance.ReStartGame ());

		ButtonPause_Resume.onClick.RemoveAllListeners ();
		ButtonPause_Resume.onClick.AddListener (() => DataManager.instance.ResumeGame ());

		ButtonPause_Pause.onClick.RemoveAllListeners ();
		ButtonPause_Pause.onClick.AddListener (() => DataManager.instance.PauseGame ());

		ButtonPause_RestartGame.onClick.RemoveAllListeners ();
		ButtonPause_RestartGame.onClick.AddListener (() => DataManager.instance.ReStartGame ());

		ButtonPause_BackToMenu.onClick.RemoveAllListeners ();
		ButtonPause_BackToMenu.onClick.AddListener (() => DataManager.instance.BackToMenu ());

		ButtonMenu_RandomEvent.onClick.RemoveAllListeners ();
		ButtonMenu_RandomEvent.onClick.AddListener (() => FunnyEventsManager.instance.RandomizeEventNumber ());

		ButtonMenu_Exit.onClick.RemoveAllListeners ();
		ButtonMenu_Exit.onClick.AddListener (() => DataManager.instance.ExitGame());

		ButtonPause_BackToMenuAfterLost.onClick.RemoveAllListeners ();
		ButtonPause_BackToMenuAfterLost.onClick.AddListener (() => DataManager.instance.BackToMenu ());

		ButtonMenu_SoundOff.onClick.RemoveAllListeners ();
		ButtonMenu_SoundOff.onClick.AddListener (() => SoundsManager.Instance.MuteAll());

		ButtonMenu_MusicOff.onClick.RemoveAllListeners ();
		ButtonMenu_MusicOff.onClick.AddListener (() => MusicManager.instance.MuteMusic());
	}
	public void  UIChangeGameState (GameState gameState)
	{
		PanelMenu.SetActive (false);
		PanelPause.SetActive (false);
		ButtonQuick_Restart.gameObject.SetActive (false);
	    PanelQuick_Restart.SetActive (false);
		ButtonPause_Pause.enabled = false;
		joystick.SetActive (false);
		//DataManager.instance.oGameState = gameState;
		switch (gameState) {
		case GameState.GameOn:
			ButtonPause_Pause.enabled = true;
			joystick.SetActive (true);
			managerOfGlobEvents.TriggerDataChange ();
			ButtonMenu_RandomEvent.transform.localScale = Vector3.zero;
			ButtonMenu_ReStart.transform.localScale = Vector3.zero;
			ButtonMenu_WatchAdd.transform.localScale = Vector3.zero;
			Menu_Title.transform.localScale = Vector3.zero;
			ButtonMenu_MusicOff.transform.localScale = Vector3.zero;
			ButtonMenu_SoundOff.transform.localScale = Vector3.zero;
			//ButtonQuick_Restart.transform.localScale = Vector3.zero;
			//ButtonPause_BackToMenuAfterLost.transform.localScale = Vector3.zero;
			//GameOverTextAsImage.transform.localScale = Vector3.zero;
			//.transform.localScale = Vector3.zero;
			break;
		case GameState.Menu:
			StopCoroutine("finishGame" );
			managerOfGlobEvents.TriggerDataChange ();
			PanelMenu.SetActive (true);
			UImanager.instance.StopCoroutine("finishGame" );
			break;
		case GameState.GamePause:
			PanelPause.SetActive (true);
			break;
		case GameState.GameLost:
		//	GameOver_prefab.SetActive (true);
			PanelQuick_Restart.SetActive (true);
			StartCoroutine (finishGame ());
			break;
		
		}
	}
	public IEnumerator finishGame(){
		GameOver_prefab.SetActive (true);
	
		ButtonQuick_Restart.gameObject.SetActive (true);
		//GameOver_prefab.transform.position = GameObject.Find ("PlaceHolder_GameOver").transform.position;

		yield return new WaitForSeconds (5f);
		if (DataManager.instance.oGameState!=GameState.Menu) {
			managerOfGlobEvents.TriggerOnChangeGameState (GameState.Menu);
		}

		Debug.Log ("FINISH GAME FINISHED");
	}
	//these two are about buying events:
	void ShowTextInComicCloud(string textToShow){
		ComicClooudInMenuTxt.text = textToShow;
		PanelBackground_Animator.SetTrigger("UnsuccesfulBought");
		Img_RandomEvent_Animator.SetTrigger("UnsuccesfulBought");
		SoundsManager.Instance.PlaySound (SoundType.NotEnoughMoney);
	}

	public void  UpdateEventDescript(){

		FunnyEventDescriptionText.text = DataManager.instance.oPlayerData.CurrentFunnyEvent.Description;
	//	Img_RandomEvent_Animator.SetTrigger("SuccesfulBought");
		changeEventImage (DataManager.instance.oPlayerData.CurrentFunnyEvent.Number.ToString ());
	}

	void changeEventImage(string eventImageName){

		Sprite newSprite =(Resources.Load<Sprite> (PREFAB_PATH + "UI_prefabs/EventsImages/" + eventImageName));
		ImageEvent_Visualization.sprite = newSprite;
	}
}

